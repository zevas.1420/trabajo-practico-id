"""core URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from RelojInteligente import views as v_reloj

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', v_reloj.inicio),
    path('reloj/altausuario', v_reloj.alta_usuario),
    path('reloj/altadispositivo', v_reloj.alta_dispositivo),
    path('reloj/altadispositivo/<int:id_dispositivo>', v_reloj.eliminar_dispositivo),
    path('reloj/altausuario/<int:id_usuario>', v_reloj.modifcar_usuario),
]
