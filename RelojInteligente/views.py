from django.shortcuts import render, redirect
from RelojInteligente.models import Dispositivo, Usuario
from RelojInteligente.helpers.cargas import cargar_usuario_base, cargar_dispositivo_base
from django.db import IntegrityError


# Create your views here.
def inicio(request):
    cantidad_usuario = Usuario.objects.count()
    cantidad_dispositivo = Dispositivo.objects.count()

    return render(request, 'index.html',{'cantidad_usuario':cantidad_usuario,'cantidad_dispositivo':cantidad_dispositivo})

def alta_usuario(request):
    lista_usuario = Usuario.objects.all()
    lista_dispositivo = Dispositivo.objects.all()
    if request.method == 'POST':
        try:
            nombre = request.POST.get('nombre')
            apellido = request.POST.get('apellido')
            dni = request.POST.get('dni')
            tipo_dispositivo_id = request.POST.get('tipo_dispositivo_id')

            cargar_usuario_base(nombre, apellido, dni, tipo_dispositivo_id)

        except IntegrityError as dbErr:
            return render(request, 'carga_usuario.html', {'lista_usuario': lista_usuario, 'error': str(dbErr)})
        except Exception as err:
            return render(request, 'carga_usuario.html',
                          {'lista_usuario': lista_usuario, 'lista_dispositivo': lista_dispositivo, 'error': str(err)})

    return render(request, 'carga_usuario.html', {'lista_usuario': lista_usuario, 'lista_dispositivo': lista_dispositivo})

def alta_dispositivo(request):
    lista_dispositivo = Dispositivo.objects.all().order_by('nombre')


    if request.method == 'POST':  # verifico que sea metodo POST lo enviado
        try:  # inicio la toma de excepciones
            nombre = request.POST.get('nombre')  #obtengo nombre del form HTML y lo agrego a la variable nombre

            cargar_dispositivo_base(nombre)  #realizo un verificador en helpers.cargas donde voy a verificar el nombre que no se repita

        except IntegrityError as dbErr:  # verifico errores de base de datos
            return render(request, 'cargar_tipoarte.html',
                          {'lista_dispositivo': lista_dispositivo, 'error': str(dbErr)})  # retorno lista y error la pagina
        except Exception as err:
            return render(request, 'carga_dispositivo.html', {'lista_dispositivo': lista_dispositivo , 'error': str(err)})

    return render(request, 'carga_dispositivo.html', {'lista_dispositivo': lista_dispositivo})  # retorno datos la pagina html


def eliminar_usuario(request,id_usuario):

    usuario = Usuario.objects.get(id=id_usuario).delete()  #el id que obtengo lo uso para eliminar
    return redirect('/reloj/altausuario') #redirecciono la pagina a cargas

def eliminar_dispositivo(request,id_dispositivo):

    dispositivo = Dispositivo.objects.get(id=id_dispositivo).delete()  #el id que obtengo lo uso para eliminar
    return redirect('/reloj/altadispositivo') #redirecciono la pagina a cargas

def modifcar_usuario(request, id_usuario):
    usuario = Usuario.objects.get(id=id_usuario)
    lista_usuario = Usuario.objects.all()
    lista_dispositivo = Dispositivo.objects.all()

    if request.method == 'POST':
            nombre = request.POST.get('nombre')
            apellido = request.POST.get('apellido')
            dni = request.POST.get('dni')
            tipo_dispositivo_id = request.POST.get('tipo_dispositivo_id')

            usuario.nombre = nombre
            usuario.apellido = apellido
            usuario.dni = dni
            usuario.tipo_dispositivo_id = tipo_dispositivo_id
            usuario.save()  # lo guardo
            return redirect('/reloj/altausuario')

    return render(request, 'modificar_usuario.html', {'usuario': usuario, 'lista_usuario': lista_usuario, 'lista_dispositivo': lista_dispositivo})

