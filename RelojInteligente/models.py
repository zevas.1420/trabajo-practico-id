from django.db import models

# Create your models here.
class Dispositivo(models.Model):
    nombre = models.CharField(max_length=100)


class Usuario(models.Model):
    nombre = models.CharField(max_length=100)
    apellido = models.CharField(max_length=100)
    dni = models.IntegerField(unique=True)
    tipo_dispositivo = models.ForeignKey(Dispositivo,on_delete=models.CASCADE, related_name='tipodispositivo')
