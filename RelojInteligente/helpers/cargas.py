from RelojInteligente.models import Usuario, Dispositivo

def cargar_dispositivo_base(nombre):
    lista_dispositivo = Dispositivo.objects.all()
    for item in lista_dispositivo:
        if item.nombre == nombre:
            raise Exception("Error: el nombre del dispositivo ya existe")
        if nombre == '':
            raise Exception("Error: el nombre no puede estar vacio")

    dispositivo = Dispositivo()
    dispositivo.nombre = nombre
    dispositivo.save()


def cargar_usuario_base(nombre,apellido,dni,tipo_dispositivo_id):
    lista_usuario = Usuario.objects.all()
    lista_dispositivo = Dispositivo.objects.all()

    for item in lista_usuario:
        if item.dni == int(dni) : #valido que no exista el dni en el listado
            raise Exception("Error: el DNI ya existe")  #errores creados
        if dni == '' : #valido que no exista el dni en el listado
            raise Exception("Error: el DNI no puede estar vacio")  #errores creados
        if nombre == '' or apellido == '':
            raise Exception("Error: el nombre o apellido no puede estar vacio")


    usuario = Usuario() #creo el artista
    usuario.nombre = nombre
    usuario.apellido = apellido
    usuario.dni = dni
    usuario.tipo_dispositivo_id = tipo_dispositivo_id
    usuario.save() # lo guardo


